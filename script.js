//NAVBAR//
let lastScrollTop = 0;
navbar = document.getElementById('navbar');


window.addEventListener('scroll', function() {
const scrollTop = window.pageTOffset ||
this.document.documentElement.scrollTop;

if (scrollTop > lastScrollTop) {
  navbar.style.top="-50px";
} else {
  navbar.style.top="0";
}
lastScrollTop = scrollTop;
});


// TYPE//
var typed = new Typed('.typed', {
    strings: ['Bonjour, je suis actuellement concepteur et développeur des applications en apprentissage à ORT Toulouse', 'Je recherche un stage pour la période du 20 juin 2021 au 25 août 2021, Docteur en philosophie et Docteur en psychologie des profondeurs, ma volonté est de mettre à contribution dans le numérique mes connssances acquises dans les Humanités, afin de fournir aux utilisateurs des applications en adéquation avec leurs désirs. '],
    typeSpeed: 20,

  });

// COMPTEUR LIVE
let compteur = 0;
$(window).scroll(function() {

  const top = $('.counter').offset().top - window.innerHeight;

  if(compteur == 0 && $(window).scrollTop() > top) {
    $('.counter-value').each(function() {
let $this = $(this),
countTo = $this.attr('data-count');
$({
  countNum: $this.text()
}).animate({
  countNum : countTo
},
    {
      duration: 10000,
      easing: 'swing',
      step: function() {
        $this.text(Math.floor(this.countNum));
      },
      complete: function() {
        $this.text(this.countNum);
      }
    });
  });
 compteur = 1;
  }
});

/*******************AOS*************/

AOS.init();

